# seq2seqfastai

https://github.com/RRisto/seq2seq

export KAGGLE_CONFIG_DIR=/exwindoz/home/juno/kaggleapi


kaggle datasets create -r tar -p data/
cd data; zip libs.zip libs/; cd ..

kaggle datasets version -p data/ -m "Updated data"


kaggle kernels push -p train/
kaggle kernels status remotejob/seq2seqfastai
kaggle kernels output remotejob/seq2seqfastai$ -p output/
cp output/model0.pt data/model0.pt
cd data
tar cfv libs.tar libs/
cd -
kaggle datasets version -r tar -p data/ -m "Updated data 0"
kaggle datasets status remotejob/seq2seqfastaidata


kaggle kernels push -p generator/
kaggle kernels status sipvip/chatbottutorialgen
kaggle kernels output sipvip/chatbottutorialgen -p output/

