import os, sys

sys.path.insert(0, "../input")
from seq2seq.data.data_manager import Seq2SeqDataManager
from seq2seq.model.seq2seq_learner import Seq2seqLearner

DEVICE = 'cuda'
MIN_LENGTH = 3
MAX_LENGTH = 9 #was 10
MIN_COUNT = 3

os.symlink("/kaggle/input/xx_ent_wiki_sm-2.1.0/xx_ent_wiki_sm", "/opt/conda/lib/python3.6/site-packages/spacy/data/xx")
## Get data
# data_manager = Seq2SeqDataManager.create_from_txt('../input/formatted_movie_lines.txt','xx', 'xx', min_freq=MIN_COUNT, min_ntoks=MIN_LENGTH,
#                                                   max_ntoks=MAX_LENGTH, switch_pair=False,device=DEVICE)
data_manager = Seq2SeqDataManager.create_from_txt('../input/formatted_movie_lines.txt','xx', 'xx', min_freq=MIN_COUNT, min_ntoks=MIN_LENGTH,
                                                  max_ntoks=MAX_LENGTH, switch_pair=False,device='cpu')


hidden_size=50
learner=Seq2seqLearner(data_manager,hidden_size)
learner.fit(10, train_batch_size=15,valid_batch_size=15,show_attention_every=5,device=DEVICE)

original_xtext = 'sama vaikkei se oikein toimi'
original_ytext = 'joo ei aina toimi pömppömaha silti'
predicted_text = learner.predict(original_xtext, device=DEVICE)
print(f'original text: {original_xtext}')
print(f'original answer: {original_ytext}')
print(f'predicted text: {predicted_text}')

original_xtext = 'ihanaa lomaa'
original_ytext = 'kiitos aurinkoisia päiviä sinullekin'
predicted_text = learner.predict(original_xtext, device=DEVICE)
print(f'original text: {original_xtext}')
print(f'original answer: {original_ytext}')
print(f'predicted text: {predicted_text}')